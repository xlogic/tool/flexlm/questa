# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

# Base image:
# https://catalog.redhat.com/software/containers/ubi9-micro/61832b36dd607bfc82e66399
ARG BASE_IMAGE_NAME=registry.access.redhat.com/ubi9-micro
ARG BASE_IMAGE_TAG=9.2-5

# Builder image
# https://catalog.redhat.com/software/containers/ubi9/618326f8c0d15aff4912fe0b
ARG BUILDER_IMAGE_NAME=registry.access.redhat.com/ubi9
ARG BUILDER_IMAGE_TAG=9.2-489

FROM ${BUILDER_IMAGE_NAME}:${BUILDER_IMAGE_TAG} AS builder

ARG FLEXLM_VERSION=11.16.4.0
ARG FLEXLM_URL=https://downloads.intel.com/akdlm/software/lic_daemons/mentor
ARG FLEXLM_FILE=Modelsim_Altera_daemon_lnx64.tar

COPY install.sh /tmp/

RUN /bin/bash /tmp/install.sh \
    --url "${FLEXLM_URL}" \
    --version "${FLEXLM_VERSION}" \
    --file "${FLEXLM_FILE}" \
    --output /usr/local/bin

FROM ${BASE_IMAGE_NAME}:${BASE_IMAGE_TAG}

ARG FLEXLM_VERSION=11.16.4.0

# http://label-schema.org/rc1/
LABEL \
    maintainer="tymoteusz.blazejczyk@tymonx.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.version="${FLEXLM_VERSION:-latest}" \
    org.label-schema.url="https://gitlab.com/xlogic/tool/flexlm/questa" \
    org.label-schema.name="flexlm-questa" \
    org.label-schema.usage="https://gitlab.com/xlogic/tool/flexlm/questa" \
    org.label-schema.vendor="xlogic.dev" \
    org.label-schema.vcs-url="https://gitlab.com/xlogic/tool/flexlm/questa" \
    org.label-schema.description="FlexLM license manager for Questa*-Intel® FPGA Edition Software" \
    org.label-schema.docker.cmd="docker run -it --rm registry.gitlab.com/xlogic/tool/flexlm/questa" \
    org.label-schema.podman.cmd="podman run -it --rm registry.gitlab.com/xlogic/tool/flexlm/questa"

COPY --from=builder /usr/bin/sed /usr/bin/sed
COPY --from=builder --chmod=0755 /usr/local/bin/* /usr/local/bin/
COPY --chmod=0755 entrypoint.sh  /usr/local/bin/
COPY --chmod=0755 healthcheck.sh /usr/local/bin/

RUN \
    ln --symbolic /lib64/ld-linux-x86-64.so.2 /lib64/ld-lsb-x86-64.so.3 && \
    echo "flexlm:x:1000:1000::/var/flexlm:/bin/bash" >> /etc/passwd && \
    echo "flexlm:x:1000:" >> /etc/group && \
    mkdir --parent                  /etc/flexlm/licenses /var/flexlm /var/run/flexlm/licenses && \
    chown --recursive flexlm:flexlm /etc/flexlm /var/flexlm /var/run/flexlm && \
    chmod --recursive o-rwx         /etc/flexlm /var/flexlm /var/run/flexlm && \
    echo "d /var/run/flexlm 0750 flexlm flexlm -" > /usr/lib/tmpfiles.d/flexlm.conf

USER flexlm:flexlm
WORKDIR /var/flexlm

HEALTHCHECK --interval=60s --timeout=60s --start-period=30s --retries=3 CMD ["/usr/local/bin/healthcheck.sh"]

# <server-port> <vendor-port>
EXPOSE 27000/tcp 1800/tcp

CMD ["lmgrd", "-z", "-c", "/var/run/flexlm/licenses"]
ENTRYPOINT ["/usr/local/bin/entrypoint.sh"]
