#!/usr/bin/env sh
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set -e

OUTPUT="."
URL="https://downloads.intel.com/akdlm/software/lic_daemons/mentor"
VERSION="11.16.4.0"
FILE="Modelsim_Altera_daemon_lnx64.tar"

print_help() {
    cat <<-EOF
Usage: ./install.sh [OPTIONS]

It downloads and installs FlexLM utilities.

Options:
  -h, --help                it prints this help message.
  -u, --url URL             download URL location.           Default: [${URL}]
  -f, --file FILE           archived file name to download.  Default: [${FILE}]
  -o, --output DIR          installation directory location. Default: [${OUTPUT}]
  -v, --version VERSION     version to download.             Default: [${VERSION}]

File to download: [${URL}/${VERSION}/${FILE}]
EOF
}

fatal() {
    >&2 echo "$*. Exiting..."; exit 1
}

parse_command_line_arguments() {
    while [ "$#" -gt 0 ]; do
        case "$1" in
            -h|--help)
                print_help; exit 0;;
            -u|--url)
                URL="${2:-${URL}}"; shift;;
            -f|--file)
                FILE="${2:-${FILE}}"; shift;;
            -o|--output)
                OUTPUT="${2:-${OUTPUT}}"; shift;;
            -v|--version)
                VERSION="${2:-${VERSION}}"; shift;;
            *)
                fatal "Unsupported option: $1";;
        esac

        shift
    done
}

main() {
    parse_command_line_arguments "$@"

    mkdir --parent "${OUTPUT}"

    curl --silent --location "${URL}/${VERSION}/${FILE}" | \
        tar --no-same-owner --verbose --extract --file - --directory "${OUTPUT}"
}

main "$@"
