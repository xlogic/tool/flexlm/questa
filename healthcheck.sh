#!/usr/bin/env sh
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set -e

main() {
    sleep 10

    output="$(lmutil lmstat -c /var/run/flexlm/licenses -lm)"
    status="$(echo "${output}" | sed -n 's/license server .* (MASTER)/\0/p')"

    case "${status}" in
        *UP*)
            ;;
        *)
            >&2 echo "License manager (lmgrd) is not UP"
            >&2 echo "${output}"
            exit 1;;
    esac

    output="$(lmutil lmstat -c /var/run/flexlm/licenses -vd)"
    status="$(echo "${output}" | sed -n 's/mgcld: .*/\0/p')"

    case "${status}" in
        *UP*)
            ;;
        *)
            >&2 echo "Vendor daemon (mgcld) is not UP"
            >&2 echo "${output}"
            exit 1;;
    esac
}

main "$@"
