#!/usr/bin/env sh
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>

set -e

get_name() {
    basename "$1"
}

get_license() {
    case "$1" in
        *.lic)
            echo "/var/run/flexlm/licenses/$1";;
        *)
            echo "/var/run/flexlm/licenses/$1.lic";;
    esac
}

get_server_port() {
    sed -nE 's/^\s*SERVER\s+[[:graph:]]+\s+[[:graph:]]+\s+([0-9]+).*$/\1/Ip' "$1"
}

get_vendor_port() {
    sed -nE 's/^\s*VENDOR\s.*(port|PORT)=([0-9]+).*$/\2/Ip' "$1"
}

get_hostname() {
    cat /proc/sys/kernel/hostname
}

get_hostid() {
    for dir in /sys/class/net/*; do
        name="$(basename "${dir}")"

        if [ "${name}" = "lo" ]; then
            continue
        fi

        tr -d ':' < "${dir}/address" | tr '[:upper:]' '[:lower:]'

        return 0
    done
}

main() {
    case "$1" in
        -*)
            set -- lmgrd -z -c "/var/run/flexlm/licenses" "$@";;
        *)
            ;;
    esac

    mkdir --parent /var/run/flexlm/licenses
    chmod --recursive o-rwx /var/run/flexlm
    chown --recursive flexlm:flexlm /var/run/flexlm

    hostname="$(get_hostname)"
    hostid="$(get_hostid)"

    for file in "${LM_LICENSE_FILE:-}" /usr/local/flexlm/licenses/* /etc/flexlm/licenses/* /var/flexlm/licenses/*; do
        if [ ! -f "${file}" ]; then
            continue
        fi

        name="$(get_name "${file}")"
        server_port="$(get_server_port "${file}")"
        vendor_port="$(get_vendor_port "${file}")"
        license="$(get_license "${name}")"

        if [ "${server_port:-0}" -eq 0 ]; then
            server_port="${FLEXLM_DEFAULT_SERVER_PORT:-27000}"
        fi

        if [ "${vendor_port:-0}" -eq 0 ]; then
            vendor_port="${FLEXLM_DEFAULT_VENDOR_PORT:-1800}"
        fi

        if [ "${vendor_port:-0}" -eq "${server_port:-0}" ]; then
            vendor_port="$((server_port + 1))"
        fi

        sed --expression="s,^\s*SERVER\(\s.*\|\)$,SERVER ${hostname} ${hostid} ${server_port},I" \
            --expression="s,^\s*VENDOR\(\s.*\|\)$,VENDOR mgcld /usr/local/flexlm/bin/mgcld port=${vendor_port},I" \
            "${file}" > "${license}"

        chmod 0640 "${license}"
        chown flexlm:flexlm "${license}"
    done

    exec "$@"
}

main "$@"
