#!/usr/bin/env sh
#
# SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
# SPDX-License-Identifier: Apache-2.0
# SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>
#
# shellcheck disable=SC2310

set -e

PROJECT_DIR="$(cd -P -- "$(dirname -- "$(command -v -- "$0" || true)")" && pwd -P)"
FILE="${PROJECT_DIR}/.gitlab-ci.yml"
LINTS=""

print_help() {
    cat <<-EOF
Usage: $0 [OPTIONS] [LINT...]

It runs linter tools used also in GitLab CI pipeline.

LINT is selected linter tool name that is defined in .gitlab-ci.yml file as *-lint.
If not provided it will run all defined linter tools.

Options:
  -h, --help        it prints this help message
  -f, --file FILE   file to read defined linter tools. Default: [${FILE}]
EOF
}

error() {
    >&2 echo "[ERROR]: $*"
}

fatal() {
    error "$*. Exiting..."; exit 1
}

parse_command_line_arguments() {
    while [ "$#" -ne 0 ]; do
        case "$1" in
            -h|--help)
                print_help; exit 0;;
            -f|--file)
                FILE="$2"; shift;;
            -*)
                fatal "Unknown argument: $1";;
            *)
                LINTS="${LINTS}${LINTS:+|}$1";;
        esac

        shift
    done
}

run() {
    podman run \
        --rm \
        --tty \
        --interactive \
        --volume "${PROJECT_DIR}:${PROJECT_DIR}:ro,z" \
        --workdir "${PROJECT_DIR}" \
        --entrypoint "" \
        "$@"
}

get_lints() {
    yq 'with_entries(select(.key | test("-lint$"))) | keys | .[]' "${FILE}" | grep -iE "(${LINTS})"
}

get_variables() {
    yq ".$1.variables | keys | .[]" "${FILE}"
}

get_variable() {
    yq ".$1.variables.$2" "${FILE}"
}

get_image() {
    kind="$(yq ".$1.image | type" "${FILE}")"

    if [ "${kind}" = "!!map" ]; then
        yq ".$1.image.name" "${FILE}"
    else
        yq ".$1.image" "${FILE}"
    fi
}

get_command() {
    yq ".$1.script[0]" "${FILE}"
}

main() {
    parse_command_line_arguments "$@"

    status="0"

    for lint in $(get_lints); do
        for variable in $(get_variables "${lint}"); do
            value="$(get_variable "${lint}" "${variable}")"
            eval "${variable}=${value}"
        done

        image="$(get_image "${lint}")"
        image="$(eval echo "${image}")"

        cmd="$(get_command "${lint}")"
        cmd="$(eval echo "${cmd}")"

        if ! output="$(run "${image}" ${cmd:+${cmd:-}} 2>&1)"; then
            >&2 echo "Failed: ${cmd}"
            >&2 echo "${output}"
            status="1"
        fi
    done

    return "${status:-0}"
}

main "$@"
