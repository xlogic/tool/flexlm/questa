# FlexLM - Questa

<!--
SPDX-FileCopyrightText: Copyright 2023 xlogic <https://xlogic.dev> and contributors to the project
SPDX-License-Identifier: Apache-2.0
SPDX-FileContributor: 2023 Tymoteusz Blazejczyk <tymoteusz.blazejczyk@tymonx.com>
-->

[[_TOC_]]

## Description

Containerized FlexLM license manager for Questa*-Intel® FPGA Edition Software.

## Features

- Containerized FlexLM license manager for Questa*-Intel® FPGA Edition Software
- Handling multiple license files
- Automatically updating `SERVER` and `VENDOR` entries in license files with proper values
- Automatically setting proper container hostname in license files required by FlexLM license manager
- Automatically setting proper container MAC address required by FlexLM license manager
- Container built-in health check for FlexLM license manager service and vendor daemon
- systemd service facilities for managing FlexLM license manager container

## Requirements

- Podman (or Docker)
- Podman plugins
- Valid license file for Questa*-Intel® FPGA Edition Software acquired via [Intel® FPGA Self-Service Licensing Center][]

Install Podman plugins to have container host names auto-resolution via dnsmasq. Example for Fedora:

```plaintext
sudo dnf install podman-plugins
```

## Podman

Create network for FlexLM license manager container:

```plaintext
podman network create --ignore <NETWORK>
```

Run FlexLM license manager container with MAC address from license file:

```plaintext
podman run -it --rm --network <NETWORK> --mac-address <MAC-ADDRESS> --hostname <NAME> --name <NAME> \
    --volume "<LICENSE-FILE>:/etc/flexlm/licenses/license.lic:ro,z" registry.gitlab.com/xlogic/tool/flexlm/questa
```

## systemd

Create directory for license files:

```plaintext
mkdir -p ~/.config/xlogic/flexlm/questa/licenses/
```

Get license file for Questa*-Intel® FPGA Edition Software acquired via [Intel® FPGA Self-Service Licensing Center][] and
copy downloaded license file to the `~/.config/xlogic/flexlm/questa/licenses` directory location:

```plaintext
cp ~/Downloads/*_License.dat ~/.config/xlogic/flexlm/questa/licenses/
```

Install FlexLM systemd template service unit:

```plaintext
curl -s "https://gitlab.com/xlogic/systemd/flexlm/-/raw/master/install.sh" | sh -s -- --user
```

Enable FlexLM systemd service unit:

```plaintext
systemctl --user enable xlogic-flexlm@questa
```

Start FlexLM systemd service unit:

```plaintext
systemctl --user start xlogic-flexlm@questa
```

Check FlexLM systemd service unit status:

```plaintext
systemctl --user status xlogic-flexlm@questa
```

Check FlexLM systemd service unit logs:

```plaintext
journalctl --pager-end --follow --user --unit xlogic-flexlm@questa
```

:warning: Note that started systemd service unit in user mode runs only during active user session.

For running systemd service unit in system mode replace the `--user` option with the `--system` option and
run all above commands via `sudo` or be login as the `root` user. All license files should be copied to the
`/etc/xlogic/flexlm/questa/licenses` directory location.

FlexLM license manager is available on network `xlogic` as `xlogic-flexlm-questa` at TCP port `27000` and Questa daemon at TCP port `1800`.

Connect Questa*-Intel® FPGA Edition Software container to `xlogic` network and set the `LM_LICENSE_FILE` environment variable:

```plaintext
podman run -it --rm --env LM_LICENSE_FILE=27000@xlogic-flexlm-questa ...
```

[Intel® FPGA Self-Service Licensing Center]: https://licensing.intel.com/psg/s/
